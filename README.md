# Webpy自动签到 v4.2.3.0

#### 重要说明

 **项目正在处于测试阶段，正在尝试重现各种用户可能性BUG并修复，请耐心等待** 

#### 介绍
使用 PHP（前端）+ Python3（自动签到/后端）进行多用户多任务的自动签到。
此项目涉及语言有：HTML、JavaScript、CSS、SQL、PHP、Python3。
目前支持签到平台：Acfun（自动领取每日香蕉）、BiliBili（每日硬币直播中心自动签到）、爱奇艺（每日签到）、网易云音乐（自动签到刷单）、QQ音乐（每日自动签到领积分）、有道云笔记签到、今日头条签到等。在一天的凌晨会自动发邮件提醒。
支持用户注册，用户邮箱验证。

#### 更新日志

2021/3/6

[=]Python 修正主页数据无法设置或设置无效的问题

2021/2/25

[=]Python 修正Python代码中数据库无法保存问题

2021/2/24

[=]PHP 修正home_page.php中查看详细日志时的css错误

[=]Python 修复天任务重复签到的问题

[+]Python 增加 有道云笔记签到、今日头条签到

2021/2/23 

[=]Python 修正数据库连接失败BUG

[+]PHP setting_page.php更新保存数据前台提交

[+]PHP 设置用户试用到期 改动 core/loader.php


#### 软件架构
大致架构为：Python3负责自动签到；PHP用于前台的用户注册、任务创建；Python与PHP通过SQL数据库互相交换数据。

#### 演示网址
项目演示网址： http://blog.zhangyishang.top/Toolbox/webpy


#### 参考的公开源码
在设计程项目（程序）时，我们参考了各路大佬源码，并进行改变应用到我们的项目，参考列表如下：
1. （Python调用SQL数据库）参考官方pymysql文档：https://www.python.org/dev/peps/pep-0249
2. （Python反爬网易云方法1）来自Github公开源码：https://github.com/SakurajimMai/wyymusic
3. （Python反爬网易云方法2）来自简书用户伪文艺boy公开的文章：https://www.jianshu.com/p/f3764544f6d6
4. （Python反爬QQ音乐获取GTK值方法）来自博客园用户jqdemo公开的文章：https://www.cnblogs.com/jqdemo/p/3139794.html
5. （Python库AES加密方法）来自CSDN用户Cosmop01itan的转载文章：https://blog.csdn.net/CosmopolitanMe/article/details/76088651?locationNum=6&fps=1
6. （HTML页面代码）来自Bootstrap 4：https://www.layoutit.com/build
7. （PHP部分写法）参考：Wordpress、CSDN、菜鸟教程、博客园、百度经验、简书等。（无法找到原地址，这里表示最诚挚的感谢！）
8. （PHP发送邮件）PHPMailer，参考菜鸟教程：https://www.runoob.com/w3cnote/php-phpmailer.html



#### 安装教程

1.  克隆此此仓库到电脑上，或者下载此仓库的全部文件。确保一下文件在克隆的文件夹内。
![1](https://images.gitee.com/uploads/images/2021/0221/163948_03084c06_5210553.png "1.png")
2.  搭建PHP环境7.3.4其它版本未经测试（可以使用宝塔面板、phpstudy等），选择一个任意的文件夹（下称“网站目录”），将PHP文件夹内所有文件全部拷贝到网站目录中。
![2](https://images.gitee.com/uploads/images/2021/0221/164534_15202c66_5210553.png "2.png")
3.  创建一个SQL数据库将，SQL文件夹中的数据用管理工具（phpMyAdmin、HeiDiSQL、SQL_Font等）导入到这个数据库内，数据库参考版本5.7.26其它版本未经测试。
![3](https://images.gitee.com/uploads/images/2021/0221/164946_c4833e84_5210553.png "3.png")
4.  确保数据库中含有webpy_log表、webpy_user表、webpy_part表和webpy_system表。分别注释如上图。打开webpy_system表，修改其中数据：
![4](https://images.gitee.com/uploads/images/2021/0221/165235_1938f8cc_5210553.png "4.png")
5.  请配置好smtp发信，Python与PHP都使用此账户发信！打开网站目录，再打开 core/loader.php 文件，进行配置PHP文件设置：
![5](https://images.gitee.com/uploads/images/2021/0221/165806_9988ade4_5210553.png "5.png")
6.  说明：变量$emailKey是Python与PHP通讯接口，下方会讲（非必要）。保存文件后，web访问此路径，若出现登录界面则PHP配置完毕。
![6](https://images.gitee.com/uploads/images/2021/0221/170127_b176d8eb_5210553.png "6.png")
7.  默认管理员账号密码均为 admin ，用此账号登录，看到如下界面并一一测试每个界面确保PHP前端正常运行。
![7](https://images.gitee.com/uploads/images/2021/0221/170531_bb960375_5210553.png "7.png")

特别说明：由于目前版本并未更新PHP用户管理，若要修改用户组，请打开数据库中的webpy_user表，修改level项，一一对应如下：-1 管理员；0 未激活用户；1 普通用户；2 试用高级用户； 3 高级用户（试用功能尚未添加）
![7.5](https://images.gitee.com/uploads/images/2021/0221/170725_eae2d7e3_5210553.png "7.5.png")

8.  配置Python环境。此源码运行在Python3上，编程使用Python3.9.0，测试在Python3.7与Python3.8也可以使用运行，其它Python3版本自行确认。打开Python文件夹下的Webpy.py安装以下库并配置其中的数据库信息与发件设置。
![8](https://images.gitee.com/uploads/images/2021/0221/171221_c6c7e1e2_5210553.png "8.png")
![8.5](https://images.gitee.com/uploads/images/2021/0221/171308_643426bf_5210553.png "8.5.png")

9.  配置完毕后运行Python出现如下输出则配置成功，且目标邮箱将会出现一封邮件。若为出现邮件或Python输出报错请重新检测填写信息。

```
[2021-02-21 17:15:06] system> 开始检查程序运行......
[2021-02-21 17:15:06] system> 正在连接数据库......
[2021-02-21 17:15:06] system> 正在测试数据库读写......
[2021-02-21 17:15:06] system> webpy_log: 数据总数 1 个  ......OK!
[2021-02-21 17:15:06] system> webpy_part: 数据总数 0 个  ......OK!
[2021-02-21 17:15:06] system> webpy_system: 数据总数 15 个  ......OK!
[2021-02-21 17:15:06] system> webpy_user: 数据总数 1 个  ......OK!
[2021-02-21 17:15:06] system> Change Data: webpy_system -> test_config  ......OK!
[2021-02-21 17:15:06] system> 正在测试网站连接......
[2021-02-21 17:15:07] system> 正在测试发邮件......
[2021-02-21 17:15:08] system> 邮件API返回详情： {'message': 'success', 'To': 'xxx@xxx.com'}
[2021-02-21 17:15:08] system> 测试发送邮件成功！
[2021-02-21 17:15:08] system> 自检已完成！所用时间： 2359.55 ms
[2021-02-21 17:15:08] system> 进入程序循环......
```


10. Python进入循环后，等待每日签到。


#### 使用说明

1.  Python发送邮件说明
Python发送邮件是使用PHP中的一个API接口进行发送邮件，此接口存在于 core/email.php 下，当然你可以自行修改Python中发邮件的方式使用Python中的smtplib库，但是太麻烦了，干脆使用PHP吧。这里奉上想要自行修改的方法，找到170行左右的send_email()函数，请不要修改函数中参数的位置（除非你想大改特改）。找到628行左右的if emailDing and emailTest判断语句，将判断语句进行修改，确保通过自检。
2.  Webpy等级制度说明
目前版本的等级制度并不完善，试用高级会员与高级会员并未实现，目前注册默认为普通用户每个项目仅可以创建3个任务，目前用户不支持找回密码功能。
3.  安全性说明
Python与PHP调用SQL数据库时会进行加密，PHP加解密函数在 core/loader.php 内，Python加解密函数在源代码内，均为 encode_string()/decode_string() ，用户密码使用md5加密与加解密函数无关。更多BUG正在修改。。。。。。
4.  文件说明
根目录的文件对应的作用为：

index.php  引导用户文件

home_page.php  自动签到首页

log_page.php   日志页面文件

setting_page.php   程序设置页面

user_page.php  用户设置页面

login_page.php  登录页面

register_page.php  注册页面

core(dir)   核心文件

css(dir)  css样式表

js(dir)  js文件



#### 参与贡献

1.  以赏的秘密小屋 http://blog.zhangyishang.top
2.  码云仓库 https://gitee.com/wojiaoyishang/webpy-auto-check-in 
3.  所有制作参考资料的同学。 
