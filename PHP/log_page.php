<?php
// 引入加载器
require_once dirname(__FILE__) . "/core/loader.php";

// 引导用户
if (!check_login()){header("Location: login_page.php");}

// 日志操作
if ($_GET['action'] == 'download'){
	Header("Content-type: application/octet-stream");
	Header("Accept-Ranges: bytes");
	Header("Accept-Length: " . strlen($user_log));
	Header("Content-Disposition: attachment; filename=Pythonlog.txt");
	echo $user_log;
}elseif ($_GET['action'] == 'clear'){
	sql_query("UPDATE `webpy_log` SET `log`='' WHERE `username` = '" . $user_name . "'");
	header("Location: /log_page.php");
}
?>

<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Python自动化程序管理后台主页 - 以赏的秘密小屋</title>

    <meta name="description" content="Python自动化程序">
    <meta name="author" content="我叫以赏">
	
	<link rel="shortcut icon" href="<?php echo $web_icon;?>" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<style>body{margin: 20px 20px 20px 20px;min-width: 1600px;} div{width : 100%;max-width: 1600px;}</style>
  </head>
  
  <body>

    <div class="container-fluid">
	<div class="row-fluid">
		<div class="col-md-12">
			<div class="alert alert-dismissable alert-info">		
			<h4>公告</h4><?php echo $dashboard_notice ; //引入公告 ?></div>
			<ul class="nav nav-pills" style="margin-bottom: 15px;">
				<li class="nav-item"><a class="nav-link" href="./index.php">数据总览页</a></li>
				<li class="nav-item"><a class="nav-link active" href="./log_page.php">日志查看页</a></li>
				<li class="nav-item"><a class="nav-link" href="./setting_page.php">程序设置页</a></li>
				<li class="nav-item"><a class="nav-link" href="./user_page.php">用户信息页</a></li>
				<p class="nav-link"><?php echo "登录用户：" . $user_name ?></p>
				
				<li class="nav-item dropdown ml-md-auto">
					 <a class="nav-link dropdown-toggle"id="navbarDropdownMenuLink" data-toggle="dropdown">其它选项</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						 
						 <a class="dropdown-item" href="http://blog.zhangyishang.top">项目首页</a> 
						 <a class="dropdown-item" href="#">赞助开发者</a> 
						 <div class="dropdown-divider"></div> 
						 <a class="dropdown-item" href="javascript:logout()">退出登录</a>
						 
					</div>
				</li>
				
			</ul>

		</div>
		
		
	<div class="container-fluid" style="margin-bottom: 15px;">
					
						
				
			
			<div class="container-fluid" style="margin-bottom: 15px;">	 
			<button type="button" class="btn btn-md btn-outline-warning" style="width: 33%;" onclick="Roll_to_the_end()">滑到底部</button>
			<button type="button" class="btn btn-outline-info" style="width: 33%;" onclick="download_log()">下载日志</button>
			<button type="button" class="btn btn-outline-danger btn-md" style="width: 33%;" onclick="delete_log()">清空日志</button>
			</div>
			
					

			<div class="row">
				<div class="col-md-12">
					<h3 id="log_total_table">程序日志 - 一共 <?php echo substr_count($user_log,"\n"); ?> 条</h3>
					<ol id="log_list">
					<?php 
						$log = explode("\n",$user_log);
						for ($i=0;$i<substr_count($user_log,"\n");$i++){
							echo '<li>' . $log[$i] . '</li>';
						}
					?>
					</ol>
				</div>
			</div>
			
			
			<div class="container-fluid" style="margin-bottom: 15px;">	 
			<button type="button" class="btn btn-md btn-outline-warning" style="width: 33%;" onclick="Roll_to_the_top()">滑到顶部</button>
			<button type="button" class="btn btn-outline-info" style="width: 33%;" onclick="download_log()">下载日志</button>
			<button type="button" class="btn btn-outline-danger btn-md" style="width: 33%;" onclick="delete_log()">清空日志</button>
			</div>


	
	
	</div>
	</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	function logout(){document.location.href = 'login_page.php?action=logout'}
	function Roll_to_the_end(){window.scrollTo(0,document.body.scrollHeight);}
	function Roll_to_the_top(){window.scrollTo(0,0);}
	function download_log(){document.location.href = 'log_page.php?action=download'}
	function delete_log(){document.location.href = 'log_page.php?action=clear'}
	</script>
  </body>
</html>