<?php
// 引入加载器
require_once dirname(__FILE__) . "/core/loader.php";

// get请求 退出登录
if ($_SERVER['REQUEST_METHOD'] == 'GET' and $_GET['action'] == 'logout'){
	setcookie("Webpyu",'',time()-1);
	setcookie("Webpyp",'',time()-1);
}


if (check_login()){echo true;header("Location: home_page.php");exit;}  // 登录有效

$warning = '';  // 登录提示

// post请求 登录
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if (trim($_POST['username']) == '' or trim($_POST['password']) == ''){
		$warning = '用户名或密码不能为空！';
	}else{
		if (sql_query("select * FROM webpy_user where username = '" . trim($_POST['username']) . "'")[1]['level'] == 0){
			$warning = '用户名或密码错误！'; // 未激活用户也是不允许登录
		}elseif (login($_POST['username'],$_POST['password'])){
			header("Location: home_page.php");
			exit;
		}else{
			$warning = '用户名或密码错误！';
		}
		
	}
}
?>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>登录Python自动化程序管理后台 - <? echo $web_title; ?></title>

    <meta name="description" content="Python自动化程序">
    <meta name="author" content="我叫以赏">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	

  </head>
  <body>

    <div class="container-fluid" style="width: 400px;box-shadow: 2px 2px 40px #00000024;border-radius: 10px;">
	<div class="row" style="margin: 10px 10px 10px 0px;margin-top: 30px;margin-bottom: 30px;">
		<div class="col-md-12" style="margin: 10px 10px 10px 0px;margin-top: 10%;margin-bottom: 30px;">
			<h3 class="text-center">用户登录</h3> 
			<p class="text-center" id="logtip"><?php echo $login_notice; ?></p>
			<p class="text-center"><span style="color:#E53333;"><strong><?php echo $warning; ?></strong></span></p>
			<form method="post" name="login">
				<div class="form-group"> 
					<label for="Username_Table">用户名</label>
					<input type="text" class="form-control" name="username" placeholder="Enter your username ......">
				</div>
				<div class="form-group">
					 <label for="Password_Table">密码</label>
					 <input type="password" class="form-control" name="password" placeholder="Enter your password ......">
				</div> 
				<button class="btn btn-primary" >验证并登录</button>
				<a href="register_page.php"><button class="btn btn-outline-success" type="button">注册新用户</button></a>
			</form>
		
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>