<?php

/* 
	我们需要一些基本信息。
	从这里开始填入你SQL数据库的信息
	$sqlServer —— 数据库网路地址
	$sqlPort —— 数据库端口
	$sqlUsername —— 数据库用户名
	$sqlPassword —— 数据库密码
	$sqlName —— 对应数据库名
*/
$sqlServer = "127.0.0.1";
$sqlPort = 3306;
$sqlUsername = "root";
$sqlPassword = "rootroot";
$sqlName = "webpy";


// 可选修改Python调用发邮件
$emailKey = "Pikachu";
// 怎么？ 喜欢皮卡丘不行？记得也要修改Python中的密匙！

/*
	保存文件后请访问站点，我们会连接数据库。
	连接成功后您就可以使用了。
	请非专业人员不要继续编辑，防止代码错误损坏。
*/


// 不报告运行时错误
ini_set("error_reporting","E_ALL & ~E_NOTICE");

// 邮件发送必要
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';


// 建立数据库的连接
$sqlConnection = mysqli_connect("p:".$sqlServer,$sqlUsername,$sqlPassword,$sqlName,$sqlPort);
if (mysqli_connect_errno($sqlConnection)) {
	die("我们无法连接数据库，请检查数据库配置请联系管理员访问 ". __FILE__ . " 修改数据库配置。<br>
		这是我们捕获到的一些数据：" . mysqli_connect_error());
}

// 设置编码，防止中文乱码
mysqli_query($sqlConnection , "set names utf8");


/* 定义函数 */


// 获取随机字符
function get_randomstr($len = 10){
	$randArr=array_merge(range(0,9),range('a','z'),range('A','Z'));
	shuffle($randArr);
	$rs=array_slice($randArr,0,$len);
	return implode($rs);
}


// 加密函数 （可自行修改 若修改了加解密函数且数据库不是第一次导入请修改对应数据库内容，密码用md5加密，不调用此函数   修改后请修改对应的Python代码）
function encode_string($string) {
	return base64_encode($string);
}

// 解密函数 （可自行修改 若修改了加解密函数且数据库不是第一次导入请修改对应数据库内容  修改后请修改对应的Python代码）
function decode_string($string) {
	return base64_decode($string);
}


// 数据库查询函数  只读一个
function sql_query($query){
	global $sqlConnection;
	$result = mysqli_query($sqlConnection , $query);
	if (mysqli_num_rows($result) == 1){
		return [mysqli_num_rows($result),mysqli_fetch_all($result, MYSQLI_ASSOC)[0]];
	}
	return [mysqli_num_rows($result),mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

// 数据库查询函数2     全部读取
function sql_query2($query){
	global $sqlConnection;
	$result = mysqli_query($sqlConnection , $query);
	return [mysqli_num_rows($result),mysqli_fetch_all($result, MYSQLI_ASSOC)];
}



// 登录
function login($username,$password) {
	if (md5($password) == sql_query("select * FROM webpy_user where username = '" . $username . "'")[1]['password']){
		setcookie("Webpyu",encode_string($username),time()+24*60*60);
		setcookie("Webpyp",md5($password),time()+24*60*60);
		return true;
	}else{
		return false;
	}
}

// 验证cookie是否有效
function check_login() {
	if (trim($_COOKIE['Webpyu']) == '') { return false; }
	if ($_COOKIE['Webpyp'] == sql_query("select * FROM webpy_user where username = '" . decode_string($_COOKIE['Webpyu']) . "'")[1]['password'] ){
		return true;
	}else{
		return false;
	}

}

// cookie有效时载入
if (check_login()){
	$user_name = decode_string($_COOKIE['Webpyu']);
	$user_regtime = sql_query("select * FROM webpy_user where username = '" . $user_name . "'")[1]['regtime'];
	$user_notice = sql_query("select * FROM webpy_user where username = '" . $user_name . "'")[1]['notice'];
	$user_level = sql_query("select * FROM webpy_user where username = '" . $user_name . "'")[1]['level'];
	$user_tryovertime = sql_query("select * FROM webpy_user where username = '" . $user_name . "'")[1]['tryovertime'];
	$user_log = sql_query("select * FROM webpy_log where username = '" . $user_name . "'")[1]['log'];
}

// 判断用户等级
if ($user_level = 2){
	if (time() > strtotime($user_tryovertime)){
		sql_query("UPDATE `webpy_user` SET `tryovertime`='1' WHERE `username` = '" . $user_name . "'");
		$user_level = 1;
	}
}



/* 读取数据库数据 */

try {
	// 一些基本的配置
    $web_verion = sql_query("select * FROM webpy_system where setting_key = 'web_version'")[1]['setting_value']; // 网站版本
	$web_title = sql_query("select * FROM webpy_system where setting_key = 'web_title'")[1]['setting_value']; // 网站标题
	$web_description = sql_query("select * FROM webpy_system where setting_key = 'web_description'")[1]['setting_value']; // 网站描述
	$web_author = sql_query("select * FROM webpy_system where setting_key = 'web_author'")[1]['setting_value']; // 网站作者
	$web_icon = sql_query("select * FROM webpy_system where setting_key = 'web_icon'")[1]['setting_value']; // 网站图标
	$login_notice = sql_query("select * FROM webpy_system where setting_key = 'login_notice'")[1]['setting_value']; // 登录公告
	$register_notice = sql_query("select * FROM webpy_system where setting_key = 'register_notice'")[1]['setting_value']; // 注册公告
	$dashboard_notice = sql_query("select * FROM webpy_system where setting_key = 'dashboard_notice'")[1]['setting_value']; // 仪表盘公告
	
	//$admin_email = sql_query("select * FROM webpy_system where setting_key = 'admin_email'")[1]['setting_value']; // 管理员邮箱
	// smtp邮箱配置
	$smtp_host = sql_query("select * FROM webpy_system where setting_key = 'smtp_host'")[1]['setting_value']; // 主机
	$smtp_port = sql_query("select * FROM webpy_system where setting_key = 'smtp_port'")[1]['setting_value']; // 端口
	$smtp_username = sql_query("select * FROM webpy_system where setting_key = 'smtp_username'")[1]['setting_value']; // 用户名
	$smtp_password = sql_query("select * FROM webpy_system where setting_key = 'smtp_password'")[1]['setting_value']; // 密码
	$smtp_name = sql_query("select * FROM webpy_system where setting_key = 'smtp_name'")[1]['setting_value']; // 发送名称
	$smtp_email = sql_query("select * FROM webpy_system where setting_key = 'smtp_email'")[1]['setting_value']; // 发送邮箱
}
catch(Exception $e) {
    die("我们成功连接了数据库，但是我们没有获取到我们想要的数据，请检查数据库数据设置，请联系管理员访问 ". __FILE__ . " 修改数据库配置。<br>
	这是我们捕获到的一些数据：" . mysqli_connect_error());
}



// 邮件发送
function send_email($To,$Name,$Subject,$Text) {
	global $smtp_host, $smtp_port, $smtp_username, $smtp_password, $smtp_name, $smtp_email;
	try { 
		$mail = new PHPMailer();  
		$mail->isSMTP();// 使用SMTP服务  
		$mail->CharSet = 'utf-8';// 编码格式为utf8，不设置编码的话，中文会出现乱码  
		$mail->Host = $smtp_host;// 发送方的SMTP服务器地址  
		$mail->SMTPAuth = true;// 是否使用身份验证  
		$mail->Username = $smtp_username;// 发送方的163邮箱用户名，就是你申请163的SMTP服务使用的163邮箱
		$mail->Password = $smtp_password;// 发送方的邮箱密码，注意用163邮箱这里填写的是“客户端授权密码”而不是邮箱的登录密码！
		$mail->SMTPSecure = 'ssl';// 使用ssl协议方式
		$mail->Port = $smtp_port;// 163邮箱的ssl协议方式端口号是465/994  
		$mail->setFrom($smtp_email,$smtp_name);// 设置发件人信息，如邮件格式说明中的发件人，这里会显示为Mailer(xxxx@163.com），Mailer是当做名字显示  
		$mail->addAddress($To,$Name);// 设置收件人信息，如邮件格式说明中的收件人，这里会显示为Liang(yyyy@163.com)  
		$mail->addReplyTo($smtp_email,$smtp_name);// 设置回复人信息，指的是收件人收到邮件后，如果要回复，回复邮件将发送到的邮箱地址  
		$mail->Subject = $Subject;// 邮件标题  
		$mail->Body = $Text;// 邮件正文  
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; //当邮件不支持html时备用显示，可以省略 
		$mail->WordWrap = 80; // 设置每行字符串的长度 
		$mail->IsHTML(true); 
		$mail->Send();
		return true; 
	} catch (phpmailerException $e) { 
		return false; 
	} 

}



?>