-- 作者： Yishang
-- 生成日期： 2021-02-21 16:09:38
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `webpy`
--

-- --------------------------------------------------------

--
-- 表的结构 `webpy_log`
--

CREATE TABLE `webpy_log` (
  `username` text COLLATE utf8_unicode_ci,
  `log` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='程序日志';

--
-- 转存表中的数据 `webpy_log`
--

INSERT INTO `webpy_log` (`username`, `log`) VALUES
('admin', '');

-- --------------------------------------------------------

--
-- 表的结构 `webpy_part`
--

CREATE TABLE `webpy_part` (
  `username` text COLLATE utf8_unicode_ci,
  `partcalled` text COLLATE utf8_unicode_ci,
  `partname` text COLLATE utf8_unicode_ci,
  `partcard` text COLLATE utf8_unicode_ci,
  `userdata` text COLLATE utf8_unicode_ci,
  `returndata` text COLLATE utf8_unicode_ci,
  `regtime` timestamp NULL DEFAULT NULL,
  `others` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='自动执行数据';

-- --------------------------------------------------------

--
-- 表的结构 `webpy_system`
--

CREATE TABLE `webpy_system` (
  `setting_key` text COLLATE utf8_unicode_ci,
  `setting_value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='系统设置';

--
-- 转存表中的数据 `webpy_system`
--

INSERT INTO `webpy_system` (`setting_key`, `setting_value`) VALUES
('web_verion', '4.2.3.0'),
('web_title', 'Python自动化'),
('web_description ', NULL),
('web_author', NULL),
('web_icon', 'http://blog.zhangyishang.top/wp-content/uploads/2020/03/cropped-blog.zhangyishang.top_2020-03-28_17-59-16-32x32.png'),
('login_notice', '登录Python自动化后台<br>\r\nHome: http://blog.zhangyishang.top'),
('dashboard_notice', '感谢您的注册，此项目正在加紧开发。您可以使用目前已有的功能，目前暂不支持密码找回，请牢记您的密码。'),
('register_notice', '注册Python自动化后台<br>\r\nHome: http://blog.zhangyishang.top'),
('smtp_username', 'example@example.com'),
('smtp_name', '以赏的秘密小屋团队'),
('smtp_email', 'example@example.com'),
('smtp_host', 'smtp.example.com'),
('smtp_port', '465'),
('smtp_password', 'example'),
('test_config', '86948');

-- --------------------------------------------------------

--
-- 表的结构 `webpy_user`
--

CREATE TABLE `webpy_user` (
  `username` text COLLATE utf8_unicode_ci,
  `password` text COLLATE utf8_unicode_ci,
  `email` mediumtext COLLATE utf8_unicode_ci,
  `regtime` timestamp NULL DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `tryovertime` timestamp NULL DEFAULT NULL,
  `notice` text COLLATE utf8_unicode_ci,
  `other` text COLLATE utf8_unicode_ci,
  `activecode` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户数据表';

--
-- 转存表中的数据 `webpy_user`
--

INSERT INTO `webpy_user` (`username`, `password`, `email`, `regtime`, `level`, `tryovertime`, `notice`, `other`, `activecode`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', NULL, '2020-12-05 04:54:45', -1, NULL, '极致的私人提示', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
